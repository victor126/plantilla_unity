﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public AudioSource song;

    private void Awake()
    {
        MouseCursor.ShowCursor();
    }

    private void Start()
    {
        song.Play();
    }

    public void PulsaPlay()
    {
        MouseCursor.HideCursor();
        SceneManager.LoadScene("Gameplay");
    }

    public void PulsaExit()
    {
        Application.Quit();
    }

    public void PulsaCredit()
    {
        SceneManager.LoadScene("Credits");
    }
}