﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSInputManager : MonoBehaviour
{
    //[SerializeField] Player player;
    [SerializeField] LookRotation lookRotation;
    [SerializeField] private float sensitivity = 3.0f;
    [SerializeField] private Vector2 mouseAxis;
    [SerializeField] private Vector2 inputAxis;
    [SerializeField] private PlayerManagement player;
    [SerializeField] Animator anim;


    private void Update()
    {
        // Modificar la camara del player

        // Rotación de la cámara
        mouseAxis = Vector2.zero; //{ 0 , 0}
        mouseAxis.x = Input.GetAxis("Mouse X") * sensitivity;
        mouseAxis.y = Input.GetAxis("Mouse Y") * sensitivity;

        lookRotation.SetRotation(mouseAxis);


        // Obtener el input
        inputAxis = Vector2.zero; //{ 0 , 0}
        inputAxis.x = Input.GetAxis("Horizontal");
        inputAxis.y = Input.GetAxis("Vertical");

        if(inputAxis.x != 0 || inputAxis.y != 0)
        {
            anim.SetBool("walking", true);
        }
        else
        {
            anim.SetBool("walking", false);
        }

        player.SetAxis(inputAxis);
        
        // Detectar el input para saltar
        if (Input.GetButtonDown("Jump"))
        {
            player.Jump();
            anim.SetBool("walking", false);
        }

        if (Input.GetButtonDown("Fire1"))
        {
            player.Fire();            
        }

        if (Input.GetButtonDown("Fire2"))
        {
            player.Fire2();
        }
        
        if(Input.GetKeyDown(KeyCode.LeftShift))
        {
            player.Run();
            anim.SetBool("correr", true);
        }
        else if(Input.GetKeyUp(KeyCode.LeftShift))
        {
            player.speed = 6f;
            anim.SetBool("correr", false);
        }

        if(player.muerte == true)
        {
            anim.SetBool("dead", true);
        }
    }
}
