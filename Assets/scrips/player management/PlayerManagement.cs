﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManagement : MonoBehaviour
{
    public int life = 5;
    private Vector2 axis;
    private CharacterController controller;      
    public float speed;
    public Vector3 moveDirection;
    public float jumpSpeed;
    private bool jump;
    public float gravityMagnitude = 1.0f;
    public bool muerte = false;
    public Vida healthBar;
    //public CanvasGroup DamageSigner;
    public float timer = 100f;

    private Vector3 transformDirection;
    public GameObject mano;
    public GameObject lanzaCohetes;

    [SerializeField] private FireTemplate bullet;
    [SerializeField] private FireTemplate rocket;


    void Start()
    {
        controller = GetComponent<CharacterController>();
    }

    void Update()
    {   
        transformDirection = axis.x * transform.right + axis.y * transform.forward;

        moveDirection.x = transformDirection.x * speed;
        moveDirection.z = transformDirection.z * speed;

        if (controller.isGrounded && !jump)
        {
            moveDirection.y = Physics.gravity.y * gravityMagnitude * Time.deltaTime;

        }
        else
        {
            jump = false;
            moveDirection.y += Physics.gravity.y * gravityMagnitude * Time.deltaTime;
        }

        controller.Move(moveDirection * Time.deltaTime);
    }


    public void SetAxis(Vector2 inputAxis)
    {
        axis = inputAxis;
    }

    public void Fire()
    {
        if(muerte == false)
        {
            Debug.Log("Fire");

            // Instanciar una pelota
            FireTemplate pelota = Instantiate(bullet, mano.transform.position, mano.transform.rotation) as FireTemplate;
            //FireTemplate rocket = Instantiate(rocket, mano.transform.position, mano.transform.rotation) as FireTemplate;

            // Ponerla en la posición del player

            // Dispararla
            pelota.Fire();
            //rocket.Fire();
        }
    }
    public void Fire2()
    {
        if(muerte == false)
        {
            Debug.Log("Fire2");

            // Instanciar una pelota
            FireTemplate cohete = Instantiate(rocket, lanzaCohetes.transform.position, lanzaCohetes.transform.rotation) as FireTemplate;

            // Ponerla en la posición del player

            // Dispararla
            cohete.Fire();
            //rocket.Fire();
        }
    }

    public void Jump()
    {
        if (muerte == false)
        {
            if (!controller.isGrounded) return;

            moveDirection.y = jumpSpeed;
            jump = true;
        }
    }
    public void Run()
    {
        if (muerte == false)
        {
            speed *= 3;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "ZombieHand")
        {
            Damage(1);
        }
    }

    void SetDead()
    {
        //sound.Play(0, 1);
        //Destroy(this.gameObject);
        muerte = true;
        Debug.Log("estoy muerto");
    }

    public void Damage(int hit)
    {
        life -= hit;
        if (life == 0) SetDead();

        healthBar.slider.value -= hit;
    }

    /*public void DamageSng()
    {
        DamageSigner.alpha = 1f;
    }*/
    /*
        if (controller.isGrounded && !jump)
            {
                moveDirection.y = forceToGround;
            }
            else
            {
                jump = false;
                moveDirection.y += Physics.gravity.y* gravityMagnitude * Time.deltaTime;
            }

            transformDirection = axis.x* transform.right + axis.y* transform.forward;

            moveDirection.x = transformDirection.x* speed;
            moveDirection.z = transformDirection.z* speed;

            controller.Move(moveDirection* Time.deltaTime);
    */
}
