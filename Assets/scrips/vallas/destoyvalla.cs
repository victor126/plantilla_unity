﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class destoyvalla : MonoBehaviour
{
    public int vidaValla = 6;


    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log(collision.gameObject.tag);
        if (collision.gameObject.tag == "Zombie")
        {
            vidaValla -= 1;
        }
    }

    void Update()
    {
        if(vidaValla <= 0)
        {
            Destroy(this.gameObject);
        }
    }
}
