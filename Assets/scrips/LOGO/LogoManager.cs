﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LogoManager : MonoBehaviour
{
    
    // Update is called once per frame
    void Update()
    {
        Invoke("ChangeScene", 8.5f);
    }

    public void ChangeScene()
    {
        SceneManager.LoadScene("Menu");
    }

}
