﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GizmosPivotPoints : MonoBehaviour
{
    // Start is called before the first frame update

    public Color color = Color.red;

    // Update is called once per frame
    private void OnDrawGizmos()
    {
        Gizmos.color = color;
        Gizmos.DrawSphere(transform.position, 1f);
    }
}
