﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SimpleEnemy : MonoBehaviour
{

    public Collider[] patrolPoints;
    private int currentPoint = 0;
    // Use this for initialization
    private NavMeshAgent agent;
    [SerializeField] Animator anim;


    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        agent.SetDestination(patrolPoints[currentPoint].gameObject.transform.position);
        anim.SetFloat("MoveSpeed", 1f);
    }

    private void OnCollisionEnter(Collision col)
    {
        Debug.Log(col.gameObject.tag);
        if (col.gameObject.tag == "valla")
        {
            Debug.Log("atacando");
            anim.SetBool("Attack", true);
        }
        else if (col.gameObject.tag == "patrol")
        {
            currentPoint++;
            currentPoint = currentPoint % patrolPoints.Length;
            agent.SetDestination(patrolPoints[currentPoint].gameObject.transform.position);
        }
    }

    private void OnCollisionExit(Collision col)
    {
        if (col.gameObject.tag == "valla")
        {
            anim.SetBool("Attack", false);
        }
    }
}
