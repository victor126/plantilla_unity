﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class FinalScore : MonoBehaviour
{
    public int currScore;
    [SerializeField] Text ScoreAmount;

    private void Start()
    {
        currScore = 0;
        UpdateScoreUI();
    }

    public void AddScore(int Amount)
    {
        currScore += Amount;

        UpdateScoreUI();
    }

    private void UpdateScoreUI()
    {
        ScoreAmount.text = currScore.ToString("0");
    }
}
