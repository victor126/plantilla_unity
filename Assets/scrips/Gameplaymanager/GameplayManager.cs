﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class GameplayManager : MonoBehaviour
{
    [SerializeField] public PlayerManagement player;
    public int currScore;
    [SerializeField] Text ScoreAmount;

    [SerializeField] private GameObject Spawner2;
    [SerializeField] private GameObject Spawner3;
    [SerializeField] private GameObject Spawner4;
    [SerializeField] private GameObject Spawner5;
    
    [SerializeField] public GameObject oleada1;
    [SerializeField] public GameObject oleada2;
    [SerializeField] public GameObject oleada3;
    [SerializeField] public GameObject oleada4;
    [SerializeField] public GameObject oleada5;

    private void Start()
    {
        currScore = 0;
        UpdateScoreUI();
    }


    // Update is called once per frame
    void Update()
    {
        if(player.muerte == true)
        {
            Debug.Log("cambiando de escena...");
            Invoke("ChangeScene", 3);
        }
    }
    void ChangeScene()
    {
        SceneManager.LoadScene("GameOver");
    }

    public void AddScore(int Amount)
    {

        currScore += Amount;
        
        if(currScore >= 100 && !Spawner2.activeSelf)
        {
            Spawner2.SetActive(true);
        }
        
        if(currScore >= 1100 && !Spawner3.activeSelf)
        {
            Spawner3.SetActive(true);
        }
        
        if(currScore >= 3200 && !Spawner4.activeSelf)
        {
            Spawner4.SetActive(true);
        }
        
        if(currScore >= 6900 && !Spawner5.activeSelf)
        {
            Spawner5.SetActive(true);
        }

        UpdateScoreUI();
    }

    private void UpdateScoreUI()
    {
        ScoreAmount.text = currScore.ToString("0");
    }
}
